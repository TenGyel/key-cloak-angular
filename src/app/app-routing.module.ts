import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DeveloperDashboardComponent } from './developer-dashboard/developer-dashboard.component';
import { AuthGuard } from './guards/auth.guard';
import { NonAuthGuard } from './guards/non-auth.guard';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { ManagerDashboardComponent } from './manager-dashboard/manager-dashboard.component';

const routes: Routes = [
    { path: '', component: LoginComponent,canActivate: [NonAuthGuard] },
    { path: 'home', component: HomeComponent,canActivate: [NonAuthGuard]},
    { path: 'developer', component: DeveloperDashboardComponent,canActivate: [AuthGuard]},
    { path: 'manager', component: ManagerDashboardComponent,canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
