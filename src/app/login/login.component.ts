import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../authentication/authentication.service';
import jwtDecode from 'jwt-decode';

export const USER_ID = "user_id";
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  tokeninfo:any;
  user_name:string = "";
  password:string = "";
  
  
  constructor(
    private router : Router,
    private authenticationService : AuthenticationService
  ) {
    
   }
  
  ngOnInit(): void {
  }
  
  login(){

    this.authenticationService.login(this.user_name, this.password).subscribe(response => {
      this.tokeninfo = jwtDecode(response.access_token);
      localStorage.setItem( USER_ID, `${this.tokeninfo.sub}`);

        console.log(this.tokeninfo.realm_access.roles)
        console.log(this.tokeninfo.sub,'User_ID')
        console.log('token expires_in',response.expires_in, "minutes")
        console.log(this.tokeninfo)

        // call autologin with timing
        this.autologin(response.expires_in*200)
        // this.initListener();

        for(var i = 0; i < this.tokeninfo.realm_access.roles.length; i++){
          switch(true){
            case this.tokeninfo.realm_access.roles[i] == "developer":
              this.router.navigate(['developer']);
              break;
            
            case this.tokeninfo.realm_access.roles[i] == "manager":
              this.router.navigate(['manager']);
              break;
            
            default:
              this.router.navigate(['/']);
          }

          // if(this.tokeninfo.realm_access.roles[i] == "developer" ){
          //   console.log("hhhhhhhhhhhhh", this.tokeninfo.realm_access.roles[i])
          //   this.router.navigate(['developer']);
          // } else if(this.tokeninfo.realm_access.roles[i] == "manager"){
          //   this.router.navigate(['manager']);
          // } else {
          //   this.router.navigate(['/']);
          // }
        }
    }); 
  }
// for refresh-token
  autologin(number){
    this.authenticationService.getRefreshTokenTime(number)
  }
}
