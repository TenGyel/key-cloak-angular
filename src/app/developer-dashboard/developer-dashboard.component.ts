import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService, REFRESH_TOKEN } from '../authentication/authentication.service';
import { CHECK_INTERVAL, MINUTES_UNITL_AUTO_LOGOUT, STORE_KEY } from '../config.constant';


@Component({
  selector: 'app-developer-dashboard',
  templateUrl: './developer-dashboard.component.html',
  styleUrls: ['./developer-dashboard.component.scss']
})
export class DeveloperDashboardComponent implements OnInit {

  constructor(private authService:AuthenticationService ,private router: Router) {
    this.initListener();
    this.authService.initInterval()
    this.authService.check()
  }

  ngOnInit(): void {
  }
  // for out-logout listener
  initListener() {
    document.body.addEventListener('click', () => this.reset());
    document.body.addEventListener('mouseover', () => this.reset());
    document.body.addEventListener('mouseout', () => this.reset());
    document.body.addEventListener('keydown', () => this.reset());
    document.body.addEventListener('keyup', () => this.reset());
    document.body.addEventListener('keypress', () => this.reset());
  }
  reset(){
    this.authService.setLastAction(Date.now());
    // console.log('reseting.....')
  }
  //end of out-logout

  logout(){
    this.authService.logout();
    this.router.navigate(["/"])
  }
}
