// Keycloak
export const CLIENT_ID = "dashboard-client";
export const GRANT_TPYE = "password";
export const SCOPE = "openid";

// refresh-token 
export const REFRESH_GRANT_TPYE = "refresh_token";

// auto-logout
export const MINUTES_UNITL_AUTO_LOGOUT = 1 // in mins
export const CHECK_INTERVAL = 60000 // in ms
export const STORE_KEY = 'lastAction';

