import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {  BehaviorSubject, Observable, tap, throwError } from 'rxjs';
import { AUTHENTICATION } from '../app.constant';
import { CHECK_INTERVAL, CLIENT_ID, GRANT_TPYE, MINUTES_UNITL_AUTO_LOGOUT, REFRESH_GRANT_TPYE, SCOPE, STORE_KEY } from '../config.constant';
import { USER_ID } from '../login/login.component';

export const TOKEN = "token";
export const REFRESH_TOKEN = "refresh_token";

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  authState = new BehaviorSubject(false);

  httpOptions = {
    headers: new HttpHeaders({
      "Content-Type": "application/x-www-form-urlencoded",
    }),
  };

  handleError(error: HttpErrorResponse) {
    return throwError(error);
  }

  constructor(private http: HttpClient, private router: Router) {
    this.check();
    this.initInterval();
   }
  // for auto-logout
  public getLastAction() {
    return parseInt(localStorage.getItem(STORE_KEY));
  }
  public setLastAction(lastAction: number) {
    localStorage.setItem(STORE_KEY, lastAction.toString());
  }

  initInterval() {
    setInterval(() => {
      this.check();
      // console.log('interval.....')
    }, CHECK_INTERVAL);
    
  }
  //check expire time
  check() {
    const now = Date.now();
    const timeleft = this.getLastAction() + MINUTES_UNITL_AUTO_LOGOUT * 60 * 1000;
    const diff = timeleft - now;
    const isTimeout = diff < 0;

    if (isTimeout) {
      localStorage.clear();
      console.log('timeout........')
      this.router.navigate(['/']);
    }
  }
  // end of auto-logout

  // for token decode
  login(username: string, password: string): Observable<any> {

    const params = new HttpParams({
      fromObject: {
        grant_type: `${GRANT_TPYE}`,
        client_id: `${CLIENT_ID}`,
        username: username,
        password: password,
        scope: `${SCOPE}`,
      }
    });

    return this.http.post(`${AUTHENTICATION}` + '/realms/dk-realm/protocol/openid-connect/token', params, this.httpOptions)
      .pipe(tap((response: any) => {
        localStorage.setItem( TOKEN, `${response.access_token}`);
        localStorage.setItem( REFRESH_TOKEN, `${response.refresh_token}`);
        return response;
      }));
  }
  // end of token decode

  // for refresh_token
  refreshTokenAutoLogin(): Observable<any> {

    const params = new HttpParams({
      fromObject: {
        grant_type: `${REFRESH_GRANT_TPYE}`,
        client_id: `${CLIENT_ID}`,
        refresh_token: localStorage.getItem('refresh_token')
      }
    });

    return this.http.post(`${AUTHENTICATION}` + '/realms/dk-realm/protocol/openid-connect/token', params, this.httpOptions)
      .pipe(tap((response: any) => {
        localStorage.setItem( TOKEN, `${response.access_token}`);
        localStorage.setItem( REFRESH_TOKEN, `${response.refresh_token}`);
        return response;
      }));
  }
  // end of refresh_token

  //for logout
  endSession(): Observable<any> {

    const params = new HttpParams({
      fromObject: {
        client_id: `${CLIENT_ID}`,
        refresh_token: localStorage.getItem('refresh_token')
      }
    });

    return this.http.post(`${AUTHENTICATION}` + '/realms/dk-realm/protocol/openid-connect/logout', params, this.httpOptions)
      .pipe(tap((response: any) => {
        return response;
      }));
  }

 //end of logout
  logout() {
    this.endSession()
    localStorage.removeItem(TOKEN);
    localStorage.removeItem(REFRESH_TOKEN);
    localStorage.removeItem(USER_ID);
    localStorage.removeItem(STORE_KEY);
  }

  isAuthenticated() {
    return this.authState.value;
  }

  getAuthenticatedToken() {
      return localStorage.getItem(TOKEN);
  }
  setItem(key: string, value: any): void {
    localStorage.setItem(key, JSON.stringify(value));
  }

  getItem(key: string): any {
    return JSON.parse(JSON.stringify(localStorage.getItem(key)));
  }

  removeItem(key: string): void {
    localStorage.removeItem(key);
  }

  // for refresh_token mili-second converion
  getRefreshTokenTime(expirationDate: number){
    setTimeout(() => {
      this.refreshTokenAutoLogin().subscribe(response=>{
        console.log(response.expires_in,'expire access_token time')
        this.getRefreshTokenTime(response.expires_in*200);
      });
    }, expirationDate);
  }
}
