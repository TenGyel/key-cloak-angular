import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../authentication/authentication.service';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  public userObject: any
  public token: any
  public refreshToken: any

  constructor(private authService:AuthenticationService, private router: Router) {
    
  }
  async ngOnInit(){
  }
  logout(){
    this.authService.logout();
    this.router.navigate(["/"])
  }
}
