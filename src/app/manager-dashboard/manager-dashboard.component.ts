import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService, REFRESH_TOKEN } from '../authentication/authentication.service';


@Component({
  selector: 'app-manager-dashboard',
  templateUrl: './manager-dashboard.component.html',
  styleUrls: ['./manager-dashboard.component.scss']
})
export class ManagerDashboardComponent implements OnInit {

  constructor(private authService:AuthenticationService, private router: Router) {
    this.authService.check()
    this.authService.initInterval()
    this.initListener()
  }

  ngOnInit(): void {
  }

  // for out-logout listener
  initListener() {
    document.body.addEventListener('click', () => this.reset());
    document.body.addEventListener('mouseover', () => this.reset());
    document.body.addEventListener('mouseout', () => this.reset());
    document.body.addEventListener('keydown', () => this.reset());
    document.body.addEventListener('keyup', () => this.reset());
    document.body.addEventListener('keypress', () => this.reset());
  }
  reset(){
    this.authService.setLastAction(Date.now());
    console.log('reseting.....')
  }
  //end of out-logout

  logout(){
    this.authService.endSession();
    this.router.navigate(["/"])
  }
}
